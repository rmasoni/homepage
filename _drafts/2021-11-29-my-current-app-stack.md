---
layout: post
categories: [Tech, macOS]
title: "My Current Everyday macOS Apps"
---

Since my [last post](/neat-little-macos-apps), I've switched my everyday apps around a bit. Here's a list of the main changes, and also some apps that I use everyday and would like to recommend.

## Keyboard Maestro

I retired [Magnet](https://magnet.crowdcafe.com) as my window manager in favor of a more versatile tool that I already owned but never got to learn and use: [Keyboard Maestro](https://www.keyboardmaestro.com). If window management is all you need, go with Magnet. If you want a complete automation tool that also does window management, Keyboard Maestro might be perfect for you too.

## Alfred

I've been using [Alfred](https://www.alfredapp.com) solely as an app launcher and contact searcher for many years because it's much faster than Spotlight. I've dabbled with some workflows, and many of them were very janky (I miss Spotlight's unit converter the most), but never went too far into Alfred's full suite of features. What a waste.

I'm glad that I finally realized I could replace two or more apps with Alfred. The first one was aText, that was one of my favorite little apps until I saw its new UI proposal — it's so atrocious it made me look for an alternative, and I found that in good old Alfred and its Snippets functionality. Granted, it's not entirely straight forward like aText as you have to resort to custom workflows for variables, but after I figured things out it does the same thing and more.

Another tool I also liked a lot but was redundant with Alfred was Maccy, the clipboard manager. Alfred has that built-in, and it works great. One less app.

# ToothFairy

This one I found by accident on some random YouTube video. It's a menubar app that basically helps you manage Bluetooth devices. My Anker headphones don't always work perfectly, and ToothFairy lets me reconnect with a click instead of turning the headphone on and off or messing around in macOS' System Preferences. The definition of <abbr title="Quality of Life">QoL</abbr>.

# SoundSource

I own most Rogue Amoeba's apps and I love them. This is another one that I couldn't see the value for some time, but now it's shining on my menubar as my default sound controller.

# Amphetamine

This is a neat little app that prevents your laptop screen from sleeping. Just like the discontinued Caffeine, it does what it says on the tin. Simple and free.

# 1Password

This is not new, I've been using 1Password as my sole password manager for many years. It's the best on the market. Affordable, reliable, easy to use, and has great integrations with browsers.

# TickTick

Despite its iffy name and developer, TickTick has become my favorite to-do list app.

I've been through Reminders, Things and most recently Todoist. Reminders got worse with every update, failing on basic features like sync. Things is incredible, but doesn't have sharing. Todoist has a great background as a company, but the app simply didn't cut it for me as many simple things like recurring tasks are done in a very strange non-standard way.

After searching a lot, I stumbled upon TickTick, and I've been using it since. TickTick has it all: good native apps, habit tracking, sharing, filters, calendar integration, detailed recurring tasks and reminders, Pomodoro and more. Highly recommended!

# Nord VPN

I switched to Nord from Express VPN a while ago, as the latter was giving me more headaches than I was willing to go through. It also has its hiccups, but for me it's been much more stable, faster and reliable.
