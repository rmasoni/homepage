---
layout: post
categories: [Work]
title: "You’re the boss, be responsible"
---


This rant isn’t directed at anyone in particular, as its very existence is only due to the fact that I have faced this situation more than one would ever want.

Here’s the typical story:

1. You have a startup and you need a freelancer to get some things done — a landing page, a mobile app, tests for your API, copy for your new homepage etc.
2. You search for people on various sites and filter through a bunch that you deem apt to do the job, and contact them (often with urgency).
3. A few of these promptly answer you and you engage in a conversation about the project and what they can actually do for you.

Maybe one will have a preliminary version or presentation of the work in a couple hours or the next day. Either way, this is my piece of advice: **get the fuck back to them**.

We see articles ever so often about how designers, developers, writers and other creatives should never let their clients waiting or unanswered. I’m certain this rule should also extend to bosses and contractors.

<figure>
<img alr="Screenshot of a busy colorful calendar" loading="lazy" src="/assets/img/posts/2015-08-03-you-are-the-boss-be-responsible/calendar-tetris.png">
<figcaption>Your startup CEO calendar. A couple more purples and we get a nice chain combo!</figcaption>
</figure>

I know that running a company isn’t easy and that your calendar looks like widescreen Tetris, but you have to realize that these professionals should also be included in your schedule.

Don’t leave _people_ unanswered. They shouldn’t be left wondering if the e-mail has reached you or not. Need more time? Tell them when you’ll be able to have a position on the subject. Keep them informed. Do not assume business is a unilateral relation where only the person getting paid has obligations. You’re giving them money, but they’re giving you value.
