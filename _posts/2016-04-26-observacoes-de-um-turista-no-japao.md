---
layout: post
categories: [Life, Japan]
language: "pt-BR"
title: "Observações de um Turista no Japão"
---


Em Abril de 2015, realizei um sonho de infância: viajar para o Japão. Vou falar sobre algumas diferenças em relação ao Brasil que teriam ajudado se soubesse delas antes de ir.

<figure>
<img alt="Foto da área comercial com temática tradicional do Aeroporto Internacional de Haneda" loading="lazy" src="/assets/img/posts/2016-04-26-observacoes-de-um-turista-no-japao/haneda-airport.jpg">
<figcaption>Haneda International Airport — © Rafael Masoni</figcaption>
</figure>


## Metrô

O metrô do Japão funciona muito bem, mas pode assustar quem está acostumado com o de São Paulo. No entanto, uma vez que você entende a dinâmica, viajar entre as estações se torna algo natural e te possibilita ir pra quase qualquer lugar do país.

<figure>
<img alt="Foto do map de estações do metrô" loading="lazy" src="/assets/img/posts/2016-04-26-observacoes-de-um-turista-no-japao/subway-map.jpg">
<figcaption>Foto por MiNe (sfmine79)</figcaption>
</figure>


## Valor da tarifa

No Japão, o valor do bilhete de metrô varia de acordo com a distância que você for percorrer. Cada estação possui um **mapa de todas as estações atendidas**, normalmente acima das máquinas que vendem o bilhete. Você deve encontrar a estação de destino nesse mapa para descobrir o preço. Então, na máquina, você escolhe o **valor da tarifa** e a **quantidade de pessoas** para pagar e receber os bilhetes. Para adiantar o processo, também é possível colocar o dinheiro antes de selecionar a passagem.


## Ajuste de tarifa

<figure>
<img alt="Foto de uma máquina de ajuste de tarifa" loading="lazy" src="/assets/img/posts/2016-04-26-observacoes-de-um-turista-no-japao/fare-machine.jpg">
</figure>

Não se preocupe se errar o valor do seu bilhete. Toda estação possui uma máquina de <em lang="en-US">Fare Adjustment</em> (ajuste de tarifa). Basta inserir seu bilhete pra receber o que pagou a mais ou completar o que pagou a menos.


## Não esqueça o bilhete

Lembre de pegar o bilhete que é devolvido **furado** logo após ser inserido na catraca. Você precisará usá-lo para sair da estação de destino.


## Escada rolante

Ao contrário daqui, deve-se **andar do lado direito** e **ficar parado do lado esquerdo** nas escadas rolantes.


## Trem-bala

Os _shinkansen_, usados para atravessar longas distâncias, possuem uma dinâmica um pouco diferente de pagamento — são **bem mais caros** e normalmente o pagamento é feito em etapas. O funcionamento também pode variar um pouco de acordo com a companhia e o trajeto, então sugiro se informar pela Internet com antecedência ou consultar uma atendente no balcão de informações para turistas das grandes estações.


## Compras

Pouquíssimos locais aceitam cartão de crédito — quase tudo é pago com dinheiro. Moedas são extremamente comuns e um bom costume é pagar o valor exato das compras, com o dinheiro trocado. Como as notas e moedas são bem diferentes umas das outras, não demora pra se acostumar.

Normalmente, **máquinas não aceitam** moedas de 5 e 1 — gaste-as em lojas.


## Carteira

Compre uma carteira Japonesa para guardar suas notas de Yen abertas, e uma moedeira para os muitos trocos que você vai movimentar.


## Lojas

<figure>
<img alt="Foto de uma bandeja para recolher dinheiro" loading="lazy" src="/assets/img/posts/2016-04-26-observacoes-de-um-turista-no-japao/money-tray.jpg">
<figcaption>Foto por Diana Schnuth</figcaption>
</figure>

Toda loja tem uma pequena bandeja na frente do caixa, e ela serve pra você colocar o dinheiro do pagamento. O atendente vai pegar o dinheiro da bandeja, te falar o quanto custou, o quanto você deu, o quanto vai te dar de troco, e contar o dinheiro na sua frente. Todas as vezes.


## Fumantes

Apesar de ter sofrido um declínio nos últimos anos devido a políticas públicas e outras iniciativas, o número de fumantes no Japão ainda é bastante alto (cerca de 20% da população).

<figure>
<img alt="Foto de uma placa anti-tabagismo com ilustrações e texto em japonês e inglês" loading="lazy" src="/assets/img/posts/2016-04-26-observacoes-de-um-turista-no-japao/smoking-sign.jpg">
<figcaption>Foto por Walter Disney</figcaption>
</figure>


## Estabelecimentos

Quase todo restaurante possui uma área para fumantes, e isso normalmente é suficiente para não incomodar quem não fuma, mas alguns não fazem um bom isolamento e o cheiro acaba escapando.


## Sinalização

Você não precisa dominar o idioma local para passar um tempo no Japão, pois até mesmo em cidades menores, quase toda sinalização e máquinas possuem uma versão em Inglês. É um tanto raro encontrar, mas em cidades como Nagoya e outras que possuem muitos imigrantes Brasileiros, também existe a versão em Português.

***

Essas são apenas algumas dicas sobre coisas que percebi durante as semanas que passei por esse país incrível. Espero que essas informações sejam úteis para sua próxima viagem!
