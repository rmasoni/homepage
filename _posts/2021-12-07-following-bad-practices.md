---
layout: post
categories: [Business, Tech]
title: "Following Bad Practices"
---

Remember when Instagram was a nice little app that let you take pictures and apply retro-style filters to share with friends? For a while, even after being acquired by Facebook in 2012, it was not much more than that. Then they had to turn it into a monstrous money-printing machine.

Today’s Instagram has single photos, carousels, ephemeral videos (Stories), livestreams, multiple content types with Reels and IGTV, and even an in-app messenger (that curiously is not Facebook Messenger).

Many other apps followed a similar path of [feature creep](https://en.wikipedia.org/wiki/Feature_creep), corrupting their original vision and complying with this corporate “fear of missing out” — in this case, “fear of failing to offer feature-parity with the big ones, upsetting our shareholders and going bankrupt”.

I feel the industry is in a downward spiral, setting its bar lower with each software release. This is sad in and of itself, but gets worse when it discourages and pressures small developers. Will there ever be another Tweetie or Sparrow that doesn’t get swallowed by big corp?
