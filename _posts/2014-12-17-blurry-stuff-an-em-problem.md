---
layout: post
categories: [Tech, CSS]
title: "Blurry Stuff — An Em Problem"
---


The confusion over nesting elements set in `em` has been over for a couple years now, so what else is there to worry about this magnificent unit we all learned to love?

<figure>
<img alt="Screenshot of zoomed in red text with poor antialiasing that reads “half pixels”" src="/assets/img/posts/2014-12-17-blurry-stuff-an-em-problem/half-pixels.png">
</figure>

Today I faced this problem while switching from SVG images to an icon font in an upcoming layout revision for [Clicksign](https://clicksign.com).

First, I thought it could be something related to the font or box size, but had no success in figuring it out until I started _commenting out the CSS of elements surrounding the icon_. I noticed that `padding` and `margin` had something to do with it.

## Tools

This little forgotten feature in Chrome’s Developer Tools helped me confirm my suspicions:

<figure>
<img alt="Screenshot of the layout inspector in Google Chrome" src="/assets/img/posts/2014-12-17-blurry-stuff-an-em-problem/layout-inspector.png">
</figure>

It turned out that `1.8em` was generating a `7.920px` padding that nudged the icon to a half pixel. [Olá Brother’s amazing Sip app](https://sipapp.io) also helped me with its quick zoom feature.

<figure>
<img alt="Screenshot of Sip’s zoom tool focusing on a badly antialiased icon" src="/assets/img/posts/2014-12-17-blurry-stuff-an-em-problem/blurred-icon.png">
</figure>

## Wrapping up

Be extra careful using `em` units when you have pixel-perfect demanding assets like icon fonts and other small glyphs. You can either fall back to `px` or fine-tune your `em` numbers to achieve round values.
