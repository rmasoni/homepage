---
layout: post
categories: [Tech, macOS]
title: "Neat little macOS apps"
---


I’ve recently stumbled upon a bunch of apps that take me back to the golden days of macOS development with names such as Tweetie, Sparrow, and many others. I highly recommend every single one of them.


## TextSniper

![Icon of a blue and purple circle with white crosshairs inside](/assets/img/posts/2020-08-31-neat-little-macos-apps/textsniper.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

This app brings OCR to macOS in the most straight forward way. The clever default shortcut <kbd title="shift + command + 2">⇧⌘2</kbd> brings out a selection tool that perfectly mimics that of macOS’ built-in screenshot functionality. Once selected, the characteristic shutter sound lets you know that whatever was inside the box has been copied to your clipboard as text.

It currently only supports English, but the very friendly developer told me in an e-mail that more languages should be coming in the future, along with new exciting features.

[Buy TextSniper ($7)](https://textsniper.app){:.link--action}


## Numi

![Icon of a white square with rounded corners and a red equal sign inside](/assets/img/posts/2020-08-31-neat-little-macos-apps/numi.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

This one has been around for some time, and I regret not buying it earlier. Numi is a very dynamic calculator that has the power to interpret natural language.

You can use variables, headings, labels, and sentences like “12.5% of what is 50,000” (for those Dragon’s Den binge-watching sessions). All of this can be organized in ‘calculations’ that are like pages with separate totals — I use them to break apart different projects or themes.

For Alfred users that miss the excellent Spotlight conversions, Numi also has a workflow that works very decently.

Numi’s most direct competitor is probably [Soulver](https://soulver.app). I’m not sure how they compare exactly, but I have the impression that Numi is a simpler — and much more affordable — counterpart. In the end, I just wish it had something like folders and better documentation (maybe in the form of autocompletion).

[Buy Numi ($19.99)](https://numi.app){:.link--action}


## Gifox

![Icon of a geometric orange fox](/assets/img/posts/2020-08-31-neat-little-macos-apps/gifox.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

If you sometimes need to quickly share gifs of your screen, Gifox is a worthy solution. There are free alternatives, like Giphy’s native app, but if you want a more native, local and streamlined experience, Gifox is the answer.

It also suggests a very familiar shortcut (<kbd title="shift + command + 6">⇧⌘6</kbd>) akin to the built-in screenshot feature for area selection, but you can also set shortcuts for window selection and many other features.

You can set the recording’s frame rate, speed, repeat count, and many other settings to achieve your ideal quality to size ratio.

If you want to store your recordings in the cloud, there are options for sharing with Dropbox, Google Drive and Imgur.

[Buy Gifox ($14.99)](https://gifox.io){:.link--action}


## aText

![Icon of a white keyboard key with a handwritten lowercase letter “a”](/assets/img/posts/2020-08-31-neat-little-macos-apps/atext.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

You can’t appreciate text expanding until you try it, and aText is the most lightweight, feature-rich, simple to use and affordable option out there.

Don’t be put off by its out of fashion website and barebones user interface — you’ll barely interact with it as aText stays out of your way, and it’s actually pretty simple and powerful.

I use this app every day in a variety of contexts: work-related e-mails and tasks, special symbols, kaomoji, and other cumbersome to type strings of text. I keep different categories organized in folders and give these folders a custom prefix that applies to all their snippets.

Everybody boasts the pioneering app [TextExpander](https://textexpander.com), but it seems to have grown into a more enterprise-focused solution, and has recently switched to a subscription-based model that doesn’t feel right to me. You could buy aText 10 times over with one year of TextExpander, and I don’t see how that is justifiable when comparing the two feature-wise.

[Buy aText ($4.99)](https://trankynam.com/atext){:.link--action}


## Magnet

![Icon of a blue circle with three white rectangles of different sizes inside](/assets/img/posts/2020-08-31-neat-little-macos-apps/magnet.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

Every Mac user must know this app already, but I think it’s worth mentioning it for the few that are unaware.

Window management in macOS isn’t the greatest, and Magnet offers simple solutions for that. With this app, you can dock windows to all sides and corners of the screen, make them maximized, centered, or move them between displays with configurable keyboard shortcuts or by dragging them to hot corners.

It’s a must-have if you want more and faster control of your windows. Unfortunately, you can’t buy it directly from the developer and must go through the Mac App Store.

[Buy Magnet ($2.99)](https://magnet.crowdcafe.com){:.link--action}


## ImageOptim

![Icon of a machine with a twisting handle on top printing out a picture](/assets/img/posts/2020-08-31-neat-little-macos-apps/imageoptim.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

This donation-supported open source app very efficiently shrinks image sizes. Just drop a bunch of images in and wait for them to take less space.

Original images are thrown in the Trash, and the resulting optimized ones are left with the same name in their place. Lossless compression is turned on by default, and there are many settings that allow you to best personalize how your images are minified.

[Get ImageOptim for free](https://imageoptim.com/mac){:.link--action}


## AppCleaner

![Icon of a grocery paper bag with a recycling symbol and app icons inside](/assets/img/posts/2020-08-31-neat-little-macos-apps/appcleaner.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

Did you know that you’re leaving a lot of junk behind when you uninstall apps by just dragging them to the Trash? Configuration files, cache and other stuff stay scattered through your file system when it should not.

With AppCleaner you just drag the app into it instead, check all the boxes, and hit Remove. And unlike its competitors, it’s free (accepts donations) and supports many versions of macOS.

[Get AppCleaner for free](https://freemacsoft.net/appcleaner){:.link--action}


## Maccy

![Icon of a pink circle with a stylized blue and teal feather inside](/assets/img/posts/2020-08-31-neat-little-macos-apps/maccy.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

Just as the developer’s description says, Maccy is a clipboard manager that keeps your copy history at hand.

You never know when you might accidentally overwrite copied content or when an unexpected crash might make you lose the stuff you were keeping in your clipboard. This app gives you peace of mind and quick access to everything you copy, giving you settings to filter out sensitive apps (like password managers) and more.

Maccy is lightweight, open-source and free, but I highly recommend supporting the developer with a donation.

[Get Maccy for free](https://maccy.app){:.link--action}


## Bonus app: Horo

![Icon of a blue hourglass with yellow sand](/assets/img/posts/2020-08-31-neat-little-macos-apps/horo.png){:style="float: right; margin: 0 0 1em 1em" width="64"}

I added this one as a bonus because I haven’t used it as much — and I should —, but it feels solid enough to be in this list.

Horo is a minimalistic time tracker that sits in your menu bar. With one click (or key combination), you’re prompted to enter the time and go. Quick and simple, and it understands natural language.

The developer offers other neat little apps like [Rocket](https://matthewpalmer.net/rocket) and [Vanilla](https://matthewpalmer.net/vanilla). All three offer a Pro version with more features.

[Get Horo for free](https://matthewpalmer.net/horo-free-timer-mac){:.link--action}


*[OCR]: Optical Character Recognition
